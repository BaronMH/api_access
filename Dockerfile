FROM python:3.9

WORKDIR /api_access

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt
COPY . .
EXPOSE 110

CMD ["python3.9","-u", "application.py"]