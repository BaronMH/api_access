import requests
import time


def request():
    my_request = requests.get(url="http://container1:105/api/v0.1/car")
    print(my_request)
    return my_request


while True:
    try:
        print("Hello box")
        api_call = request()
        print(api_call.json())
    except (ConnectionRefusedError, Exception) as e:
        print("Connection Refused", e)
    time.sleep(10)
