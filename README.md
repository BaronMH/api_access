# README #


### How do I get set up? ###

* docker build /Users/baron/PycharmProjects/api_access/ -t api_service
* docker build /Users/baron/PycharmProjects/bmh_samcart -t api_box
* docker run -p 105:105 --name container1 api_box
* docker run -p 110:110 --name container2 api_service

### Create Network for Nodes to Communicate ###

* docker network create myNetwork
* docker network connect myNetwork container1
* docker network connect myNetwork container2
* docker network inspect myNetwork
